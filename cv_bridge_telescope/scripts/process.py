#!/usr/bin/env python
from __future__ import print_function

import roslib
roslib.load_manifest('cv_bridge_telescope')
import sys
import rospy
import cv2
import numpy as np
import math
from std_msgs.msg import String, Int32 # Messages used in the node must be imported.
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class image_converter:

  def __init__(self):
    self.image_pub = rospy.Publisher("image_topic_2",Image)

    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("video_to_topic",Image,self.callback)

  def callback(self,data):
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError as e:
      print(e)

    solBajo1 = np.array([0,0,255], np.uint8)
    solAlto1 = np.array([255,4,255], np.uint8)

    solBajo2 = np.array([0,0,255], np.uint8)
    solAlto2 = np.array([0,0,255], np.uint8)

    frameHSV = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)

    #Colocar los rangos para cada mascara

    mask1 = cv2.inRange(frameHSV, solBajo1, solAlto1)
    mask2 = cv2.inRange(frameHSV, solBajo2, solAlto2)

    #Unimos las mascaras
    #Para tener un mayor rango de valores

    mask = cv2.add(mask1,mask2)
    
    #Dibujamos un punto en centro del conjunto de datos (REFERENCIA)

    cv_image=cv2.circle(cv_image,(682,384),5,(255,0,0),-1)

    #Seguimiento del sol por su contorno y rango de valores

    (_,contours,_) = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for c in contours:
       contour= max(contours,key=cv2.contourArea)
       area = cv2.contourArea(c)
       #Delimitamos los valores del area del contorno a utilizar
       if area > 500 and area < 6500:
         pub = rospy.Publisher('sun_tracking', Pose, queue_size=10)
         target_pose = Pose()

	 #Declaramos las variables a utilizar
	 #Distancia de las coordenadas del sol desde el punto central (posicion de la camara)
         x_d=0.0
         y_d=0.0
	 #Posicion de las coordenadas previamente (para posteriormente comparar)
         x_d_p=0.0
         y_d_p=0.0
	 #Valores de los angulos de las coordenadas del sol en radianes
         ang_x=0.0
         ang_y=0.0
	 distance=0.0

	 x,y,w,h = cv2.boundingRect(contour)

         if (x+w)/(y+h) >= 1 and (x+w)/(y+h) <= 3:
	   cv_image = cv2.rectangle(cv_image,(x,y),(x+w,y+h),(255,0,0),2)

	 cv_image = cv2.circle(cv_image,((2*x+w)/2,(2*y+h)/2),5,(255,0,0),-1)
	 cv_image = cv2.line(cv_image,(682,384),((2*x+w)/2,(2*y+h)/2),(0,255,0),2)

	 #Calculamos la distancia de X y Y desde el centro de la imagen
	 x_d = (((2*x+w)/2)-682)
	 y_d = -(((2*y+h)/2)-384)
         
         #Definimos las variables para el Modelo de proyeccion
         #Distancia focal de la camara
         f=1.3 
         #Distancia al sol (se aproxima, asumiendo un valor grande)
         Z=10000000 
         #Calculamos el factor de proyeccion
         ph=Z/f
         #Proyeccion para obtener coordenadas reales del sol
         X=x_d*ph
         Y=y_d*ph
         
         #Calculamos la distancia desde el centro de la imagen
         #hasta las coordenadas del sol
	 distance = round(np.sqrt(pow(X,2)+pow(Y,2)),4)

	 #Dividimos el rango de la trayectoria del sol en los cuadrantes I y II
	 #Para el cuadrante I
	 if X < 0 and Y > 0:
		ang_x = round(math.asin(X/distance),2)
         	ang_y = round(math.asin(Y/distance),2)

	 #Para el cuadrante II
	 if Y > 0 and Y > 0:
		ang_x = round(math.asin(X/distance),2)
         	ang_y = round(math.asin(Y/distance),2)

         #Enviamos los valores de los angulos
         #Cada vez que la trayectoria avanza 5 pixeles en "X"
         #En este caso, se aumentan esos pixeles a las coordenadas reales
         #multiplicando por el 5 por el factor de proyeccion
         if abs(X-x_d_p) > 5*ph:
                 target_pose.position.x = -ang_x
                 target_pose.position.y = -ang_y
		 target_pose.position.z = 0.0
                 #Publica los valores de los angulos para la POSE del telescopio
		 pub.publish(target_pose)
		 #Actualiza la posicion del sol	
		 x_d_p = X
		 y_d_p = Y

    #cv2.imshow("Mask window", mask)
    #cv2.imshow("Image window", cv_image)

    cv2.waitKey(3)

    try:
      self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
    except CvBridgeError as e:
      print(e)

def main(args):
  ic = image_converter()
  rospy.init_node('image_converter', anonymous=True)
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
